import mongoose from "mongoose";

let isConnected = false ; //track se connection status

export const connectToDB = async () => {

    //set up mongoose pour eviter des warnings dans la console
    mongoose.set('strictQuery', true);

    //on vérifie si on est connecté
    if(isConnected){
        console.log('MongoDB is already connected')
        //le fait de mettre un return arrête la fonction (comme un break)
        return;
    }

    try {
        //on tente la connection à la base
        await mongoose.connect(process.env.MONGODB_URI, {
            dbName: "share_prompt",
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })

        //si ça marche on peut passer isConnected en true
        isConnected = true;

        console.log("MongoDB connected")

    } catch (error) {
        console.log(error);
    }
}